package jun1;

import java.util.StringJoiner;

public class IzmenaRegularna extends Izmena {
    private TipRegularneIzmene tipIzmene;

    IzmenaRegularna(Zaglavlje zaglavlje, String poruka, TipRegularneIzmene tipIzmene, int id){
        super(zaglavlje,poruka,id);
        this.tipIzmene = tipIzmene;
    }

    IzmenaRegularna(Zaglavlje zaglavlje, String poruka, TipRegularneIzmene tipIzmene){
        super(zaglavlje, poruka);
        this.tipIzmene = tipIzmene;
    }

    @Override
    public String toString() {
        return "[ir] " + super.toString() + " " + tipIzmene + "\n" + getPoruka();
    }

    @Override
    public String serijalizuj(){
        StringJoiner sj = new StringJoiner(", ");
        sj.add("ir");
        sj.add(getZaglavlje().getAutor());
        sj.add("" + getId());
        sj.add(getZaglavlje().getVremenskaOznaka());
        sj.add(getPoruka());
        sj.add("" + tipIzmene.uBroj());
        return sj.toString();
    }
}
