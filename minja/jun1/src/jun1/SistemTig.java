package jun1;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class SistemTig extends Application {

    List<Izmena> izmene = new ArrayList<>();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        VBox root = new VBox(10);
        root.setPadding(new Insets(10,10,10,10));

        HBox hbTop = new HBox(10);
        TextArea textArea = new TextArea();
        HBox hbBottom = new HBox(10);
        root.getChildren().addAll(hbTop,textArea,hbBottom);

        Button buttonUcitaj = new Button("Ucitaj");
        Button buttonSacuvaj = new Button("Sacuvaj");
        hbTop.getChildren().addAll(buttonUcitaj,buttonSacuvaj);

        VBox vbLeft = new VBox(10);
        VBox vbRight = new VBox(10);
        hbBottom.getChildren().addAll(vbLeft,vbRight);

        Label lblAutor = new Label("Autor");
        TextField tfAutor = new TextField();
        Label lblPoruka = new Label("Poruka");
        TextField tfPoruka = new TextField();
        Label lblDatum = new Label("Datum");
        TextField tfDatum = new TextField();
        vbLeft.getChildren().addAll(lblAutor,tfAutor,lblPoruka,tfPoruka,lblDatum,tfDatum);

        RadioButton rb1 = new RadioButton("Izmenjena funkcionalnost");
        RadioButton rb2 = new RadioButton("Ispravljen bag");
        RadioButton rb3 = new RadioButton("Baterija testova");
        ToggleGroup tg = new ToggleGroup();
        rb1.setToggleGroup(tg);
        rb2.setToggleGroup(tg);
        rb3.setToggleGroup(tg);
        rb1.setSelected(true);
        Button buttonDodaj = new Button("Dodaj");
        vbRight.getChildren().addAll(rb1,rb2,rb3,buttonDodaj);

        buttonUcitaj.setOnAction(e -> {
            izmene.clear();
            textArea.clear();
            Izmena.postaviSledeciId(1);

            try {
                List<String> linije = Files.readAllLines(Paths.get("izmene.txt"));
                for (String linija: linije) {
                    System.out.println("Parsiram '" + linija + "'");
                    String[] tokeni = linija.split(",");
                    String tip = tokeni[0].trim();
                    String autor = tokeni[1].trim();
                    int id = Integer.parseInt(tokeni[2].trim());
                    System.out.println("id=" + id);
                    String datum = tokeni[3].trim();
                    String poruka = tokeni[4].trim();

                    System.out.println();

                    Zaglavlje z = new Zaglavlje(autor, datum);

                    Izmena i = null;
                    if (tip.equals("ir")) {
                        // IzmenaRegularna
                        int tipIzmene = Integer.parseInt(tokeni[5].trim());
                        i = new IzmenaRegularna(z, poruka, TipRegularneIzmene.izBroja(tipIzmene),id);
                        izmene.add(i);
                    } else if (tip.equals("iz")) {
                        // IzmenaZahtev
                        i = new IzmenaZahtev(z, poruka, id);
                        izmene.add(i);
                    } else if (tip.equals("ipz")) {
                        // IzmenaPrihvatanjeZahteva
                        int idZahteva = Integer.parseInt(tokeni[5].trim());
                        i = new IzmenaPrihvatanjeZahteva(z, poruka, id, idZahteva);
                        izmene.add(i);
                    } else {
                        System.out.println("Greska pri parsiranju linije '" + linija + "'");
                    }
                }

                izmene.sort((i1, i2) -> Integer.compare(i2.getId(), i1.getId()));

                textArea.clear();
                izmene.forEach(i->textArea.appendText(i.toString()+"\n\n"));

                // Azuriramo poslednji dostupni id
                int poslednjId = izmene.get(0).getId();
                Izmena.postaviSledeciId(poslednjId + 1);

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        buttonSacuvaj.setOnAction(e->{
            StringBuilder sb = new StringBuilder();
            for(Izmena izmena : izmene) sb.append(izmena.serijalizuj()).append('\n');

            Path outputPath = Paths.get("izmene.txt");
            try {
                Files.write(outputPath,sb.toString().getBytes());
                textArea.setText("Podaci sacuvani u izmene.txt");
            } catch (IOException ex) {
                System.out.println("Neuspelo pisanje!");
            }
        });

        buttonDodaj.setOnAction(e->{
            String autor,datum,poruka;
            if(tfAutor.getText().isEmpty() || tfPoruka.getText().isEmpty() || tfDatum.getText().isEmpty())return;
            autor = tfAutor.getText().trim();
            datum = tfDatum.getText().trim();
            poruka = tfPoruka.getText().trim();

            Zaglavlje zaglavlje = new Zaglavlje(autor,datum);

            TipRegularneIzmene tipIzmene;

            if(rb1.isSelected())tipIzmene = TipRegularneIzmene.IzmenjenaFunkcionalnost;
            else if (rb2.isSelected())tipIzmene = TipRegularneIzmene.IspravljenBag;
            else tipIzmene = TipRegularneIzmene.BaterijaTestova;

            Izmena izmena =  new IzmenaRegularna(zaglavlje,poruka,tipIzmene);
            izmene.add(0,izmena);

            textArea.clear();
            izmene.forEach( i -> textArea.appendText(i.toString()+"\n\n"));

        });

        Scene scene = new Scene(root, 500, 500);
        primaryStage.setTitle("The Tig");
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
