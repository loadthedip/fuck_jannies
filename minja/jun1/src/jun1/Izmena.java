package jun1;

public abstract class Izmena {

    private Zaglavlje zaglavlje;
    private String poruka;
    private int id;

    private static int sledeciSlobodniId = 1;

    Izmena(Zaglavlje zaglavlje, String poruka, int id){
        this.zaglavlje = zaglavlje;
        this.poruka = poruka;
        this.id = id;
    }

    Izmena(Zaglavlje zaglavlje, String poruka){
        this.zaglavlje = zaglavlje;
        this.poruka = poruka;
        this.id = sledeciSlobodniId++;
    }

    public Zaglavlje getZaglavlje() {
        return zaglavlje;
    }

    public String getPoruka() {
        return poruka;
    }

    public int getId() {
        return id;
    }

    public static void postaviSledeciId(int id){
        sledeciSlobodniId=id;
    }

    @Override
    public String toString() {
        return zaglavlje + " #" + id;
    }

    public abstract String serijalizuj();
}
