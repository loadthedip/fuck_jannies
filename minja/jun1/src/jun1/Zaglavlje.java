package jun1;

public class Zaglavlje {
    private String autor;
    private String vremenskaOznaka;

    Zaglavlje(String autor, String vremenskaOznaka){
        this.autor = autor;
        this.vremenskaOznaka = vremenskaOznaka;
    }

    @Override
    public String toString() {
        return autor + " " + vremenskaOznaka;
    }

    public String getAutor() {
        return autor;
    }

    public String getVremenskaOznaka() {
        return vremenskaOznaka;
    }
}
