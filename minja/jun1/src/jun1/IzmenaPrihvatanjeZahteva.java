package jun1;

import java.util.StringJoiner;

public class IzmenaPrihvatanjeZahteva extends Izmena {
    private int idPrihvacenogZahteva;

    IzmenaPrihvatanjeZahteva(Zaglavlje zaglavlje, String poruka, int idPrihvacenogZahteva, int id){
        super(zaglavlje,poruka,id);
        this.idPrihvacenogZahteva = idPrihvacenogZahteva;
    }

    IzmenaPrihvatanjeZahteva(Zaglavlje zaglavlje, String poruka, int idPrihvacenogZahteva){
        super(zaglavlje,poruka);
        this.idPrihvacenogZahteva = idPrihvacenogZahteva;
    }

    @Override
    public String toString() {
        return "[ipz] " + super.toString() + " za #" + idPrihvacenogZahteva;
    }

    @Override
    public String serijalizuj(){
        StringJoiner sj = new StringJoiner(", ");
        sj.add("ir");
        sj.add(getZaglavlje().getAutor());
        sj.add("" + getId());
        sj.add(getZaglavlje().getVremenskaOznaka());
        sj.add(getPoruka());
        sj.add("" + idPrihvacenogZahteva);
        return sj.toString();
    }
}
