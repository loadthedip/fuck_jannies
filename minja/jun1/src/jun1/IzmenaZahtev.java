package jun1;

import java.util.StringJoiner;

public class IzmenaZahtev extends Izmena {

    IzmenaZahtev(Zaglavlje zaglavlje, String poruka, int id){
        super(zaglavlje,poruka,id);
    }

    IzmenaZahtev(Zaglavlje zaglavlje, String poruka){
        super(zaglavlje, poruka);
    }

    @Override
    public String toString() {
        return "[iz] " + super.toString();
    }

    @Override
    public String serijalizuj(){
        StringJoiner sj = new StringJoiner(", ");
        sj.add("iz");
        sj.add(getZaglavlje().getAutor());
        sj.add("" + getId());
        sj.add(getZaglavlje().getVremenskaOznaka());
        sj.add(getPoruka());
        return sj.toString();
    }
}
