package jun1;

public enum TipRegularneIzmene {

    IzmenjenaFunkcionalnost, IspravljenBag, BaterijaTestova;

    public static TipRegularneIzmene izBroja(int i){
        if(i==3) return BaterijaTestova;
        if(i==2) return IspravljenBag;
        return IzmenjenaFunkcionalnost;
    }

    public int uBroj(){
        if(this==BaterijaTestova) return 3;
        if(this==IspravljenBag) return 2;
        return 1;
    }
}
