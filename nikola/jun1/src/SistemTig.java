import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.StringJoiner;

// Input fajl je input.txt
// Output jalt je output.txt

public class SistemTig extends Application {
    private static final List<Izmena> izmene = new ArrayList<>();
    @Override
    public void start(Stage stage) {
        VBox root = new VBox(10);
        root.setPadding(new Insets(10));

        HBox hboxTop = new HBox(10);
        Button btnUcitaj = new Button("Ucitaj");
        Button btnSacuvaj = new Button("Sacuvaj");

        hboxTop.getChildren().addAll(btnUcitaj,btnSacuvaj);

        root.getChildren().add(hboxTop);

        TextArea taIspis = new TextArea();
        root.getChildren().add(taIspis);

        HBox hboxBot = new HBox(10);

        VBox botLeft = new VBox(10);
        VBox botRight = new VBox(10);

        Label lblAutor = new Label("Autor");
        TextField tfAutor = new TextField();
        Label lblPoruka = new Label("Poruka");
        TextField tfPoruka = new TextField();
        Label lblDatum = new Label("Datum");
        TextField tfDatum = new TextField();

        botLeft.getChildren().addAll(lblAutor,tfAutor,lblPoruka,tfPoruka,lblDatum,tfDatum);

        RadioButton rbtnNovaFunk = new RadioButton("Nova funkcionalnost");
        RadioButton rbtnIsprBag = new RadioButton("Ispravljen bag");
        RadioButton rbtnBatTest = new RadioButton("Baterija testova");
        Button btnDodaj = new Button("Dodaj");
        Button btnExit = new Button("Exit");
        Label lblSacuvano = new Label();

        rbtnBatTest.setSelected(true);

        ToggleGroup tg = new ToggleGroup();
        rbtnBatTest.setToggleGroup(tg);
        rbtnIsprBag.setToggleGroup(tg);
        rbtnNovaFunk.setToggleGroup(tg);

        botRight.getChildren().addAll(rbtnNovaFunk,rbtnIsprBag,rbtnBatTest,btnDodaj,lblSacuvano,btnExit);

        hboxBot.getChildren().addAll(botLeft,botRight);

        root.getChildren().add(hboxBot);

        btnSacuvaj.setOnAction(actionEvent -> {
            Path outputFile = Paths.get("output.txt");
            List<String> linije = new ArrayList<>();

            for (Izmena izmena :
                    izmene) {
                linije.add(izmena.serijalizuj());
            }

            try {
                Files.write(outputFile, linije);
            } catch (IOException e) {
                e.printStackTrace();
                System.err.println("Error writing to file!");
            }

            lblSacuvano.setText("Sacuvano!");
        });

        btnUcitaj.setOnAction(actionEvent -> {
            List<String> linije = new ArrayList<>();
            Path inputFile = Paths.get("input.txt");

            try {
                linije = Files.readAllLines(inputFile);
            } catch (IOException e) {
                e.printStackTrace();
                System.err.println("Greska pri citanju fajla.");
            }


            for (String linija :
                    linije) {
                izmene.add(genIzmenaObj(linija));
            }

            izmene.sort(Comparator.reverseOrder());
            Izmena.setSledeciSlobodanId(izmene.get(0).getId()+1);

            StringJoiner sj = new StringJoiner("\n");

            for (Izmena izmena :
                    izmene) {
                sj.add(izmena.toString() + "\n");
            }

            taIspis.setText(sj.toString());

        });

        btnDodaj.setOnAction(actionEvent -> {
            Zaglavlje novoZaglavlje = new Zaglavlje(
                    tfAutor.getText(),
                    tfDatum.getText()
            );

            TipRegularneIzmene tip = null;
            if(rbtnBatTest.isSelected()) {
                tip = TipRegularneIzmene.BaterijaTestova;
            }
            if(rbtnIsprBag.isSelected()) {
                tip = TipRegularneIzmene.IspravljenBag;
            }
            if(rbtnNovaFunk.isSelected()) {
                tip = TipRegularneIzmene.NovaFunkcionalnost;
            }

            izmene.add(
                    new IzmenaRegularna(
                            novoZaglavlje,
                            tfPoruka.getText().trim(),
                            tip
                    )
            );

            System.err.println(izmene.get(izmene.size()-1).serijalizuj());

        });

        btnExit.setOnAction(e -> Platform.exit());

        Scene scene = new Scene(root,500,500);
        stage.setScene(scene);
        stage.setTitle("The Tig");
        stage.show();

    }

    public static Izmena genIzmenaObj(String linija) {
        String[] tokeni = linija.split(",");
        System.err.println(linija);

        Zaglavlje zaglavlje = new Zaglavlje(
                tokeni[Polje.AUTOR.getVrednost()].trim(),
                tokeni[Polje.DATUM.getVrednost()].trim()
        );

        String poruka = tokeni[Polje.PORUKA.getVrednost()].trim();
        int id = Integer.parseInt(tokeni[Polje.ID.getVrednost()].trim());

        switch(tokeni[Polje.VRSTA.getVrednost()]) {
            case "ir":
                TipRegularneIzmene tip = TipRegularneIzmene.izBroja(
                        Integer.parseInt(tokeni[Polje.TIP.getVrednost()].trim())
                );
                return new IzmenaRegularna(zaglavlje,poruka,id,tip);
            case "iz":
                return new IzmenaZahtev(zaglavlje,poruka,id);
            case "ipz":
                int prihvacen_id = Integer.parseInt(
                        tokeni[Polje.TIP.getVrednost()].trim()
                );
                return new IzmenaPrihvatanjeZahteva(zaglavlje,poruka,id,prihvacen_id);
        }

        return null;
    }

    public static void main(String[] args) {
        launch();
    }
}
