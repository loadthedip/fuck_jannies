public enum Polje {
    VRSTA(0),
    AUTOR(1),
    ID(2),
    DATUM(3),
    PORUKA(4),
    TIP(5);

    private final int vrednost;

    Polje(int vrednost) {
        this.vrednost = vrednost;
    }

    public int getVrednost() {
        return vrednost;
    }
}
