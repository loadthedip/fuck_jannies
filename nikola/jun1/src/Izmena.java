public abstract class Izmena implements Comparable {
    private Zaglavlje zaglavlje;
    private String poruka;
    private Integer id;

    private static int sledeciSlobodanId;

    public Izmena(Zaglavlje zaglavlje, String poruka, Integer id) {
        this.zaglavlje = zaglavlje;
        this.poruka = poruka;
        this.id = id;
    }

    public Izmena(Zaglavlje zaglavlje, String poruka) {
        this.zaglavlje = zaglavlje;
        this.poruka = poruka;
        this.id = sledeciSlobodanId++;
    }

    public Zaglavlje getZaglavlje() {
        return zaglavlje;
    }

    public String getPoruka() {
        return poruka;
    }

    public Integer getId() {
        return id;
    }

    public static void setSledeciSlobodanId(int sledeciSlobodanId) {
        Izmena.sledeciSlobodanId = sledeciSlobodanId;
    }

    public abstract String serijalizuj();

    @Override
    public int compareTo(Object o) {
        if(this.id < ((Izmena) o).getId()) {
            return -1;
        }
        else if(this.id > ((Izmena) o).getId()) {
            return 1;
        }
        return 0;
    }
}
