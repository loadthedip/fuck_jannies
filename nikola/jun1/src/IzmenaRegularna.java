import java.util.StringJoiner;

public class IzmenaRegularna extends Izmena {

    private TipRegularneIzmene tipIzmene;

    public IzmenaRegularna(Zaglavlje zaglavlje, String poruka, int id, TipRegularneIzmene tipIzmene) {
        super(zaglavlje, poruka, id);
        this.tipIzmene = tipIzmene;
    }

    public IzmenaRegularna(Zaglavlje zaglavlje, String poruka, TipRegularneIzmene tipIzmene) {
        super(zaglavlje, poruka);
        this.tipIzmene = tipIzmene;
    }

    @Override
    public String toString() {
        return "[ir] " + getZaglavlje().toString() + " #" + super.getId() +" " + tipIzmene + "\n" + getPoruka();
    }

    @Override
    public String serijalizuj() {
        StringJoiner sj = new StringJoiner(",");
        sj.add("ir");
        sj.add(getZaglavlje().getAutor());
        sj.add(getId().toString());
        sj.add(getZaglavlje().getVremenskaOznaka());
        sj.add(getPoruka());
        sj.add(Integer.toString(tipIzmene.uBroj()));
        return sj.toString();
    }
}
