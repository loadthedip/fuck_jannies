public enum TipRegularneIzmene {

    NovaFunkcionalnost,
    IspravljenBag,
    BaterijaTestova;

    public static TipRegularneIzmene izBroja(int i) {
        switch(i) {
            case 2: return IspravljenBag;
            case 3: return BaterijaTestova;
        }
        return NovaFunkcionalnost;
    }

    public int uBroj() {
        switch(this) {
            case IspravljenBag: return 2;
            case BaterijaTestova: return 3;
        }

        return 1;
    }

}
