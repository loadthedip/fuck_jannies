public class Zaglavlje {

    private String autor,vremenskaOznaka;

    public Zaglavlje(String autor, String vremenskaOznaka) {
        this.autor = autor;
        this.vremenskaOznaka = vremenskaOznaka;
    }

    public String getVremenskaOznaka() {
        return vremenskaOznaka;
    }

    public String getAutor() {
        return autor;
    }

    @Override
    public String toString() {
        return autor + " " + vremenskaOznaka;
    }
}
