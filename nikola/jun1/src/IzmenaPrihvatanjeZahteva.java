import java.util.StringJoiner;

public class IzmenaPrihvatanjeZahteva extends Izmena {
    private int idPrihvacenogZahteva;

    public IzmenaPrihvatanjeZahteva(Zaglavlje zaglavlje, String poruka, Integer id, int idPrihvacenogZahteva) {
        super(zaglavlje, poruka, id);
        this.idPrihvacenogZahteva = idPrihvacenogZahteva;
    }

    public IzmenaPrihvatanjeZahteva(Zaglavlje zaglavlje, String poruka, int idPrihvacenogZahteva) {
        super(zaglavlje, poruka);
        this.idPrihvacenogZahteva = idPrihvacenogZahteva;
    }

    @Override
    public String toString() {
        return "[ipz] " + getZaglavlje().toString() + " #" + getId() + " za " + "#" + idPrihvacenogZahteva + "\n" + getPoruka();
    }

    @Override
    public String serijalizuj() {
        StringJoiner sj = new StringJoiner(",");
        sj.add("ipz");
        sj.add(getZaglavlje().getAutor());
        sj.add(Integer.toString(getId()));
        sj.add(getZaglavlje().getVremenskaOznaka());
        sj.add(getPoruka());
        sj.add(Integer.toString(idPrihvacenogZahteva));
        return sj.toString();
    }
}
