import java.util.StringJoiner;

public class IzmenaZahtev extends Izmena {
    public IzmenaZahtev(Zaglavlje zaglavlje, String poruka, Integer id) {
        super(zaglavlje, poruka, id);
    }

    public IzmenaZahtev(Zaglavlje zaglavlje, String poruka) {
        super(zaglavlje, poruka);
    }

    @Override
    public String toString() {
        return "[iz] " + getZaglavlje().toString() + " #" + super.getId() +"\n" + getPoruka();
    }

    @Override
    public String serijalizuj() {
        StringJoiner sj = new StringJoiner(",");
        sj.add("iz");
        sj.add(super.getZaglavlje().getAutor());
        sj.add(Integer.toString(super.getId()));
        sj.add(super.getZaglavlje().getVremenskaOznaka());
        sj.add(super.getPoruka());
        return sj.toString();
    }
}
